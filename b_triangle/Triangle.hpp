#ifndef __TRIANGLE__
#define __TRIANGLE__

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>
#include <stdexcept>
#include <functional>
#include <vector>

class Triangle {
public:
    void run();
private:
    const uint32_t WIDTH = 800;
    const uint32_t HEIGHT = 500;
    struct QueueFamilyIndices {
        int graphicsFamily = -1;
        int presentFamily = -1;
        bool isComplete()
        {
            return graphicsFamily >= 0 && presentFamily >= 0;
        }
    };
    struct SwapChainSupportDetails {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };
    VkInstance instance;
    VkSurfaceKHR surface;
    GLFWwindow* window;
    VkDebugReportCallbackEXT callback;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    VkDevice device;
    VkQueue graphicsQueue, presentQueue;
    VkSwapchainKHR swapChain;
    std::vector<VkImage> swapChainImages;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    QueueFamilyIndices *indices = nullptr;
    std::vector<VkImageView> swapChainImageViews;
    VkRenderPass renderPass;
    VkPipelineLayout pipelineLayout;
    VkPipeline graphicsPipeline;
    VkCommandPool commandPool;
    std::vector<VkCommandBuffer> commandBuffers;
    std::vector<VkFramebuffer> swapChainFramebuffers;
    const std::vector<const char*> validationLayers = {
        "VK_LAYER_GOOGLE_threading",
        "VK_LAYER_LUNARG_parameter_validation",
        "VK_LAYER_LUNARG_object_tracker",
        "VK_LAYER_LUNARG_core_validation",
        "VK_LAYER_GOOGLE_unique_objects"
    };
    const std::vector<const char*> deviceExtensions = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };
#ifdef NDEBUG
    const bool enableValidationLayers = false;
#else
    const bool enableValidationLayers = true;
#endif
    VkSemaphore imageAvailableSemaphore;
    VkSemaphore renderFinishedSemaphore;
//     SwapChainSupportDetails details;
    
    bool checkValidationLayerSupport();
    void cleanup();
    void createInstance();
    std::vector<const char*> getRequiredExtensions();
    void initWindow();
    void initVulkan();
    void mainLoop();
    VkResult CreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback);
    void DestroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator);
    void pickPhysicalDevice();
    bool checkDeviceExtensionSupport(VkPhysicalDevice);
    bool isDeviceSuitable(VkPhysicalDevice);
    int rateDevice(VkPhysicalDevice);
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice);
    void createLogicalDevice();
    void setupDebugCallback();
    void createSurface();
    void createSwapChain();
    void createImageViews();
    void createRenderPass();
    void createGraphicsPipeline();
    void createFramebuffers();
    void createCommandPool();
    void createCommandBuffers();
    void createSemaphores();
    void drawFrame();
    VkShaderModule createShaderModule(const std::vector<char>&);
    std::vector<char> readFile(const std::string&);
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>&);
    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR&);
    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice);
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objType,
        uint64_t obj,
        size_t location,
        int32_t code,
        const char* layerPrefix,
        const char* msg,
        void* userData);
    void recreateSwapChain();
    void cleanupSwapChain();
    static void onWindowResize(GLFWwindow *window, int width, int height)
    {
        Triangle* app = reinterpret_cast<Triangle*>(glfwGetWindowUserPointer(window));
        app->recreateSwapChain();
    }
};

#endif
