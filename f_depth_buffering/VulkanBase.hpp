#ifndef __VULKAN_BASE__
#define __VULKAN_BASE__

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>
#include <stdexcept>
#include <functional>
#include <vector>
#include <string>
#include <array>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "stb_image.h"

#include <chrono>

class VulkanBase
{
public:
    void run();

private:
    struct Vertex {
        glm::vec3 pos;
        glm::vec3 color;
        glm::vec2 texCoord;
        
        static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescription()
        {
            std::array<VkVertexInputAttributeDescription, 3> attributeDescription = {};
            attributeDescription[0].binding = 0;
            attributeDescription[0].location = 0;
            attributeDescription[0].format = VK_FORMAT_R32G32B32_SFLOAT;
            attributeDescription[0].offset = offsetof(Vertex, pos);
            
            attributeDescription[1].binding = 0;
            attributeDescription[1].location = 1;
            attributeDescription[1].format = VK_FORMAT_R32G32B32_SFLOAT;
            attributeDescription[1].offset = offsetof(Vertex, color);
            
            attributeDescription[2].binding = 0;
            attributeDescription[2].location = 2;
            attributeDescription[2].format = VK_FORMAT_R32G32_SFLOAT;
            attributeDescription[2].offset = offsetof(Vertex, texCoord);
            
            return attributeDescription;
        }
    };
    struct UniformBufferObject {
        glm::mat4 model;
        glm::mat4 view;
        glm::mat4 proj;
    };
    const std::vector<Vertex> vertices = {
        {{-0.8f, -0.42f, 0.0f}, {1.0f, 0.5f, 0.5f}, {1.0f, 0.0f}},
        {{0.8f, -0.42f, 0.0f}, {0.5f, 1.0f, 0.5f}, {0.0f, 0.0f}},
        {{0.8f, 0.42f, 0.0f}, {0.5f, 0.5f, 1.0f}, {0.0f, 1.0f}},
        {{-0.8f, 0.42f, 0.0f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}},
        
        {{-0.8f, -0.42f, -0.5f}, {1.0f, 0.5f, 0.5f}, {1.0f, 0.0f}},
        {{0.8f, -0.42f, -0.5f}, {0.5f, 1.0f, 0.5f}, {0.0f, 0.0f}},
        {{0.8f, 0.42f, -0.5f}, {0.5f, 0.5f, 1.0f}, {0.0f, 1.0f}},
        {{-0.8f, 0.42f, -0.5f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}
    };
    const std::vector<uint16_t> indices = {
        0, 1, 2, 2, 3, 0,
        4, 5, 6, 6, 7, 4
    };
    struct SwapChainSupportDetails {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };
    struct QueueFamilyIndices {
        int graphicsFamily = -1;
        int presentFamily = -1;
        bool isComplete() {
            return graphicsFamily >= 0 && presentFamily >= 0;
        }
    };
    const char* TEXTURE_PATH = "textures/emma-stone.jpg";
    const uint32_t HEIGHT = 450, WIDTH = 800;
    GLFWwindow *window;
    VkInstance instance;
    VkSurfaceKHR surface;
    VkPhysicalDevice physicaldevice = VK_NULL_HANDLE;
    VkDevice device;
    VkQueue graphicQueue, presentQueue;
    QueueFamilyIndices *qFamilyIndices = nullptr;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    VkSwapchainKHR swapChain;
    std::vector<VkImage> swapChainImages;
    std::vector<VkImageView> swapChainImageViews;
    VkSemaphore imageAvailableSemaphore, renderFinishedSemaphore;
    VkRenderPass renderPass;
    VkDescriptorSetLayout descriptorSetLayout;
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
    std::vector<VkFramebuffer> swapChainFrameBuffers;
    VkCommandPool commandPool;
    VkImage textureImage;
    VkDeviceMemory textureImageMemory;
    VkImageView textureImageView;
    VkSampler textureSampler;
    VkImage depthImage;
    VkDeviceMemory depthImageMemory;
    VkImageView depthImageView;
    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;
    VkBuffer indexBuffer;
    VkDeviceMemory indexBufferMemory;
    VkDescriptorPool descriptorPool;
    std::vector<VkDescriptorSet> descriptorSets;
    std::vector<VkBuffer> uniformBuffers;
    std::vector<VkDeviceMemory> uniformBuffersMemory;
    std::vector<VkCommandBuffer> commandBuffers;
#if NDEBUG
    bool enableValidationLayers = false;
#else
    bool enableValidationLayers = true;
#endif
    const std::vector<const char*> validationLayers = {
        "VK_LAYER_GOOGLE_threading",
        "VK_LAYER_LUNARG_parameter_validation",
        "VK_LAYER_LUNARG_object_tracker",
        "VK_LAYER_LUNARG_core_validation",
        "VK_LAYER_GOOGLE_unique_objects"
    };
    const std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
    VkDebugReportCallbackEXT callback;

    void initWindow();
    void initVulkan();

    // Vulkan initialization things.
    void createInstance();
    VkResult createDebugReportCallbackEXT ( VkInstance instance, const VkDebugReportCallbackCreateInfoEXT *pCreateInfo, VkAllocationCallbacks *pAllocator, VkDebugReportCallbackEXT *pCallback );
    void destroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator);
    void setupDebugCallback();
    void createSurface();
    void pickPhysicalDevice();
    void createLogicalDevice();

    //Swap chain
    void createSwapChain();
    void createImageViews();
    void createRenderpass();
    void createDescriptorLayout();
    void createGraphicsPipeline();
    void createGraphicsPipelineLayout();
    void createFrameBuffers();
    void createCommandPool();
    void createTextureImage();
    void createTextureImageView();
    void createTextureSampler();
    void createDepthResources();
    void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory);
    VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);
    void transitionImageLayout(VkImage, VkFormat, VkImageLayout, VkImageLayout);
    void createBuffer(VkDeviceSize, VkBufferUsageFlags, VkMemoryPropertyFlags, VkBuffer&, VkDeviceMemory&);
    void copyBuffer(VkBuffer, VkBuffer, VkDeviceSize);
    void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
    VkCommandBuffer beginSingleTimeCommands();
    void endSingleTimeCommands(VkCommandBuffer);
    void createVertexBuffer();
    void createIndexBuffer();
    void createUniformBuffer();
    void createDescriptorPool();
    void createDescriptorSets();
    void createCommandBuffers();
    void createSemaphores();

    void mainLoop();
    void cleanup();

    void drawFrame();
    void updateUniformBuffer(uint32_t);
    void recreateSwapChain();
    void cleanupSwapChain();
    static void onWindowResize(GLFWwindow*, int, int);
    bool checkValidationLayerSupport();
    VkFormat findDepthFormat();
    VkFormat findSupportedFormat(const std::vector<VkFormat>&, VkImageTiling, VkFormatFeatureFlags);
    bool hasStencilComponent(VkFormat);
    std::vector<const char*> getRequiredExtensions();
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objType,
        uint64_t obj,
        size_t location,
        int32_t code,
        const char* layerPrefix,
        const char* msg,
        void* userData);
    int ratePhysicalDevice(VkPhysicalDevice);
    bool checkDeviceExtensionSupport(VkPhysicalDevice);
    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice);
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice);
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>);
    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR&);
    std::vector<char> readFile(const std::string&);
    VkShaderModule createShaderModule(const std::vector<char>&);
    VkVertexInputBindingDescription getBindingDescription();
    uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
};

#endif
