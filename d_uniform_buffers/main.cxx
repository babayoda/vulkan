#include "VulkanBase.hpp"

int main()
{
    VulkanBase app;
    try {
        app.run();
    } catch(const std::runtime_error& e) {
        std::cerr << e.what() << std::endl;
        return -1;
    }
    return 0;
}
